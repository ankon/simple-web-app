package com.ankon.simplewebapp.restcontrollers;

import com.ankon.simplewebapp.customclass.PrescriptionClass;
import com.ankon.simplewebapp.repositories.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/prescriptions")
public class PrescriptionRestController {
    private PrescriptionRepository prescriptionRepository;

    @Autowired
    public PrescriptionRestController (PrescriptionRepository prescriptionRepository) {
        this.prescriptionRepository = prescriptionRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<PrescriptionClass> getAll() {
        return prescriptionRepository.findAll();
    }

    @RequestMapping(value = "/by-date/{fromDate}", method = RequestMethod.GET)
    public List<PrescriptionClass> retrievePrescriptions(@PathVariable String fromDate) {
        List<PrescriptionClass> filteredPrescriptions = new ArrayList<PrescriptionClass>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date parseFromDate = sdf.parse(sdf.format(fromDate));
            Date parsePrescriptionDate;
            for (PrescriptionClass prescription : prescriptionRepository.findAll()) {
                parsePrescriptionDate = sdf.parse(sdf.format(prescription.getPrescriptionDate()));
                if (parseFromDate.after(parsePrescriptionDate)) {
                    filteredPrescriptions.add(prescription);
                }
            }
        } catch (DateTimeException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return filteredPrescriptions;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public List<PrescriptionClass> create(@RequestBody PrescriptionClass prescriptionClass) {
        prescriptionRepository.save(prescriptionClass);

        return prescriptionRepository.findAll();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public List<PrescriptionClass> remove(@PathVariable int id) {
        prescriptionRepository.delete(id);

        return prescriptionRepository.findAll();
    }
}
