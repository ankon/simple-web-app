package com.ankon.simplewebapp.customclass;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class PrescriptionClass {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    private Date prescriptionDate;
    private String name;
    private int age;
    private String gender;
    private String diagnosis;
    private String medicines;
    private Date nextVisit;

    public PrescriptionClass() {}

    public PrescriptionClass(Date prescriptionDate, String name, int age, String gender) {
        this.prescriptionDate = prescriptionDate;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.diagnosis = "";
        this.medicines = "";
        this.nextVisit = null;
    }

    public PrescriptionClass(Date prescriptionDate, String name, int age, String gender, String diagnosis, String medicines, Date nextVisit) {
        this.prescriptionDate = prescriptionDate;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.diagnosis = diagnosis;
        this.medicines = medicines;
        this.nextVisit = nextVisit;
    }

    public Date getPrescriptionDate() {
        return prescriptionDate;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getId() {
        return id;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setMedicines(String medicines) {
        this.medicines = medicines;
    }

    public void setNextVisit(Date nextVisit) {
        this.nextVisit = nextVisit;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getMedicines() {
        return medicines;
    }

    public Date getNextVisit() {
        return nextVisit;
    }
}
