package com.ankon.simplewebapp.repositories;

import com.ankon.simplewebapp.customclass.UserClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserClass, Long> {
}
