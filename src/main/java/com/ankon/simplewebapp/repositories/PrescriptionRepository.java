package com.ankon.simplewebapp.repositories;

import com.ankon.simplewebapp.customclass.PrescriptionClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepository extends JpaRepository<PrescriptionClass, Integer> {

}
