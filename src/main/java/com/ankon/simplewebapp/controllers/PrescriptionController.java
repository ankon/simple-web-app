package com.ankon.simplewebapp.controllers;

import com.ankon.simplewebapp.customclass.PrescriptionClass;
import com.ankon.simplewebapp.repositories.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("username")
public class PrescriptionController {
    @Autowired
    PrescriptionRepository prescriptionRepository;

    @RequestMapping(value = "/list-prescriptions")
    public String showPrescriptions(ModelMap model) {
        String username = (String) model.get("username");
        return "list-prescriptions";
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String prescriptionForm(Model model) {
        model.addAttribute("prescription", new PrescriptionClass());
        return "form";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String prescriptionCreate(@ModelAttribute PrescriptionClass prescription, Model model) {
        model.addAttribute("prescription", prescription);

        System.out.print(prescription.getName() + " " + prescription.getAge() + " " + prescription.getDiagnosis());

        this.prescriptionRepository.save(prescription);

        return "list-prescriptions";
    }

    @RequestMapping(value = "/edit-form/{id}", method = RequestMethod.GET)
    public String prescriptionEditForm(@PathVariable int id, Model model) {
        model.addAttribute("prescription", prescriptionRepository.findOne(id));
        return "edit-form";
    }

    @RequestMapping(value = "/edit-form/{id}", method = RequestMethod.POST)
    public String prescriptionEdit(@ModelAttribute PrescriptionClass prescription, @PathVariable int id, Model model) {
        model.addAttribute("prescription", prescription);
        this.prescriptionRepository.delete(id);
        this.prescriptionRepository.save(prescription);
        return "list-prescriptions";
    }
}
