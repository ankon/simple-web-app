package com.ankon.simplewebapp.seeds;

import com.ankon.simplewebapp.customclass.PrescriptionClass;
import com.ankon.simplewebapp.customclass.UserClass;
import com.ankon.simplewebapp.repositories.PrescriptionRepository;
import com.ankon.simplewebapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class PrescriptionSeed implements CommandLineRunner {
    private PrescriptionRepository prescriptionRepository;

    @Autowired
    public PrescriptionSeed(PrescriptionRepository prescriptionRepository) {
        this.prescriptionRepository = prescriptionRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<PrescriptionClass> prescriptions = new ArrayList<>();

        prescriptions.add(new PrescriptionClass(new Date(), "Patient 1", 30, "male"));
        prescriptions.add(new PrescriptionClass(new Date(), "Patient 2", 25, "female"));

        prescriptionRepository.save(prescriptions);
    }
}
