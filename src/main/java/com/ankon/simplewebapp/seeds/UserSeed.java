package com.ankon.simplewebapp.seeds;

import com.ankon.simplewebapp.customclass.UserClass;
import com.ankon.simplewebapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserSeed implements CommandLineRunner{
    private UserRepository userRepository;

    @Autowired
    public UserSeed(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<UserClass> users = new ArrayList<>();

        users.add(new UserClass("user1", "password1"));
        users.add(new UserClass("user2", "password2"));

        userRepository.save(users);
    }
}
