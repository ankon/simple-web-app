(function () {
    'use strict';

    angular
        .module('app')
        .controller('PrescriptionRestController', PrescriptionRestController);

    PrescriptionRestController.$inject = ['$http'];

    function PrescriptionRestController($http) {
        var vm = this;

        vm.prescriptions = [];
        vm.getAll = getAll;
        vm.retrievePrescriptions = retrievePrescriptions;
        vm.deletePrescription = deletePrescription;

        init();

        function init() {
            getAll();
            console.log(vm.prescriptions);
        }

        function getAll() {
            var url = "/prescriptions/all"
            var prescriptionsPromise = $http.get(url);
            prescriptionsPromise.then(function(response){
                vm.prescriptions = response.data;
            });
        }

        function retrievePrescriptions(fromDate) {
            var url = "/prescriptions/by-date/" + fromDate;
            var prescriptionsPromise = $http.get(url);
            prescriptionsPromise.then(function(response){
                vm.prescriptions = response.data;
            });
        }

        function deletePrescription(id) {
            var url = "/prescriptions/delete/" + id;
            $http.post(url).then(function(response){
                vm.prescriptions = response.data;
            });
        }
    }
})();